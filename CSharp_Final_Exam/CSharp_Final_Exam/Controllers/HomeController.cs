﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CSharp_Final_Exam.Data;
using Microsoft.AspNetCore.Mvc;
using CSharp_Final_Exam.Models;
using CSharp_Final_Exam.Models.EstablishmentViewModels;
using CSharp_Final_Exam.Models.HomeViewModels;
using CSharp_Final_Exam.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace CSharp_Final_Exam.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _environment;
        private readonly FileUploadService _fileUploadService;

        public HomeController(
            UserManager<ApplicationUser> userManager, 
            ApplicationDbContext context, 
            IHostingEnvironment environment, 
            FileUploadService fileUploadService)
        {
            _userManager = userManager;
            _context = context;
            _environment = environment;
            _fileUploadService = fileUploadService;
        }

        [TempData]
        public string Message { get; set; }

        public async Task<IActionResult> Index(int page = 1)
        {
            IndexViewModel model = await DoPaging(page, null);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SearchEstablishments(string keyWord, int page = 1)
        {
            IndexViewModel model = await DoPaging(page, keyWord);
            return View("Index", model);
        }

        public IActionResult CreateEstablishment()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateEstablishment(RegisterEstablishmentViewModel model)
        {
            ApplicationUser user = await _userManager.FindByNameAsync(User.Identity.Name);

            Establishment establishment = new Establishment
            {
                Title = model.Title,
                Description = model.Description,
                UserId = user.Id,
            };

            EntityEntry<Establishment> addedEstablishment = await _context.Establishments.AddAsync(establishment);
            
            await _context.SaveChangesAsync();
            int establishmentId = addedEstablishment.Entity.Id;

            establishment.MainPhoto = UploadImage(establishmentId, model.Photo);
            _context.Establishments.Update(establishment);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> IndexPagingAjax(int page = 1)
        {
            IndexViewModel model = await DoPaging(page, null);
            return View("IndexPagingAjax", model);
        }

        public async Task<IActionResult> ViewEstablishment(int establishmentId)
        {
            Establishment establishment = await _context.Establishments.FirstOrDefaultAsync(e => e.Id == establishmentId);
            IQueryable<Image> images = _context.Images.Where(i => i.EstablishmentId == establishmentId);
            IQueryable<Review> reviews =
                _context.Reviews.Where(r => r.EstablishmentId == establishmentId).Include(r => r.User);

            double averageRatings = 0;
            if (reviews.Any())
            {
                averageRatings = reviews.Average(r => r.Rating);
            }

            EstablishmentProfileViewModel model = new EstablishmentProfileViewModel
            {
                Establishment = establishment,
                AverageRating = averageRatings,
                Reviews = reviews.ToList(),
                EstablishmentId = establishment.Id,
                Images = images.ToList(),
                Message = Message,
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateReview(EstablishmentProfileViewModel model, int newRating)
        {
            ApplicationUser user = await _userManager.FindByNameAsync(User.Identity.Name);
            Review review = new Review
            {
                DateOfCreation = DateTime.Now.Date,
                Comment = model.NewReview,
                EstablishmentId = model.EstablishmentId,
                Rating = newRating,
                UserId = user.Id
            };

            await _context.Reviews.AddAsync(review);
            await _context.SaveChangesAsync();

            Message = "Your review has been successfully added";

            return RedirectToAction("ViewEstablishment");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadNewPhoto(EstablishmentProfileViewModel model)
        {
            ApplicationUser user = await _userManager.FindByNameAsync(User.Identity.Name);

            Image image = new Image
            {
                EstablishmentId = model.EstablishmentId,
                UserId = user.Id,
                ImagePath = UploadImage(model.EstablishmentId, model.NewPhoto),
            };

            await _context.Images.AddAsync(image);
            await _context.SaveChangesAsync();

            Message = "New photo has been successfully uploaded";

            return RedirectToAction("ViewEstablishment");
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private string UploadImage(int establishmentId, IFormFile file)
        {
            var path = Path.Combine(
                _environment.WebRootPath,
                $"images\\{establishmentId}");

            _fileUploadService.Upload(path, file.FileName, file);

            return $"images/{establishmentId}/{file.FileName}";
        }

        private async Task<IndexViewModel> DoPaging(int page, string keyWord)
        {
            IQueryable<Establishment> establishments;

            switch (keyWord)
            {
                case null:
                    establishments = _context.Establishments;
                    break;
                default:
                    establishments =
                        _context.Establishments.Where(e => e.Title.Contains(keyWord) || e.Description.Contains(keyWord));
                    break;
            }
            
            List<EstablishmentMainIfnoViewModel> establishmentInfos = new List<EstablishmentMainIfnoViewModel>();

            foreach (var establishment in establishments)
            {
                IEnumerable<Review> reviews = _context.Reviews.Where(r => r.EstablishmentId == establishment.Id);
                double averageRatings = 0;
                if (reviews.Any())
                {
                    averageRatings = reviews.Average(r => r.Rating);
                }
                
                int imagesCounts = await _context.Images.CountAsync(i => i.EstablishmentId == establishment.Id);

                EstablishmentMainIfnoViewModel mainInfoModel = new EstablishmentMainIfnoViewModel
                {
                    Establishment = establishment,
                    AverageRatings = averageRatings,
                    ReviewsCounts = reviews.Count(),
                    ImageCounts = imagesCounts
                };

                establishmentInfos.Add(mainInfoModel);
            }

            int pageSize = 20;
            var count = establishmentInfos.Count();
            var items = establishmentInfos.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            PagingViewModel pageViewModel = new PagingViewModel(count, page, pageSize);

            IndexViewModel model = new IndexViewModel
            {
                EstablishmentInfos = items,
                PagingViewModel = pageViewModel
            };

            return model;
        }

    }
}
