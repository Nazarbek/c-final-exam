﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace CSharp_Final_Exam.Models.EstablishmentViewModels
{
    public class RegisterEstablishmentViewModel
    {
        [Required(ErrorMessage = "Please, enter the title")]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please, enter the description")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required(ErrorMessage = "No file chosen")]
        [Display(Name = "Main photo")]
        public IFormFile Photo { get; set; }
    }
}
