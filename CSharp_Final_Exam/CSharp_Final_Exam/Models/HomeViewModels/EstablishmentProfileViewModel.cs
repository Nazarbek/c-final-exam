﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using CSharp_Final_Exam.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CSharp_Final_Exam.Models.HomeViewModels
{
    public class EstablishmentProfileViewModel
    {
        public Establishment Establishment { get; set; }
        public List<Image> Images { get; set; }
        public double AverageRating { get; set; }
        public List<Review> Reviews { get; set; }
        
        [HiddenInput]
        public int EstablishmentId { get; set; }

        [Required(ErrorMessage = "Please, enter your review")]
        [Display(Name = "Add review")]
        public string NewReview { get; set; }

        [Required(ErrorMessage = "Please, enter your rating")]
        [Display(Name = "Rating")]
        public SelectList RatingList { get; set; }

        [Required(ErrorMessage = "No file chosen")]
        [Display(Name = "Upload new photo")]
        public IFormFile NewPhoto { get; set; }

        public string Message { get; set; }

        readonly List<int> _ratings = new List<int> { 1, 2, 3, 4, 5 };

        public EstablishmentProfileViewModel()
        {
            RatingList = new SelectList(_ratings);
        }
    }
}
