﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CSharp_Final_Exam.Models.HomeViewModels
{
    public class EstablishmentMainIfnoViewModel
    {
        public Establishment Establishment { get; set; }
        public int ReviewsCounts { get; set; }
        public double AverageRatings { get; set; }
        public int  ImageCounts { get; set; }
    }
}
