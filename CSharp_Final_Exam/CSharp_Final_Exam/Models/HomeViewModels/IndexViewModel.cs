﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CSharp_Final_Exam.Models.HomeViewModels
{
    public class IndexViewModel
    {
        public PagingViewModel PagingViewModel { get; set; }
        public List<EstablishmentMainIfnoViewModel> EstablishmentInfos { get; set; }
    }
}
