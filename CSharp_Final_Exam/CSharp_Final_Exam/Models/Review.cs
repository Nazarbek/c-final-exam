﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CSharp_Final_Exam.Models
{
    public class Review
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }   
        public int EstablishmentId { get; set; }
        public Establishment Establishment { get; set; }
        public DateTime DateOfCreation { get; set; }
        public string Comment { get; set; }
        public int Rating { get; set; }
    }
}
