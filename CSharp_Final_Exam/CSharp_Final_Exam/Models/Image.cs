﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CSharp_Final_Exam.Models
{
    public class Image
    {
        public int Id { get; set; }
        public string ImagePath { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public int EstablishmentId { get; set; }
        public Establishment Establishment { get; set; }
    }
}
