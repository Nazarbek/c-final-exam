﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CSharp_Final_Exam.Models.AccountViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Please, enter your email")]
        [EmailAddress(ErrorMessage = "Enter correct email format")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
}
